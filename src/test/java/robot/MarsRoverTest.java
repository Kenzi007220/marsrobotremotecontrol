package robot;


import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import robot.domain.Coordinates;
import robot.service.Direction;
import robot.service.Highland;

public class MarsRoverTest {

    //Init
    Highland highland = new Highland(5,5);

    private static final Logger logger = LoggerFactory.getLogger(MarsRoverTest.class);
    @Test
    public void canProvideCurrentLocationAsString() {
        //Given
        Coordinates startingPosition = new Coordinates(3,3);

        //When
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //then
        Assert.assertEquals("3 3 N", marsRover.currentLocation());
    }

    @Test
    public void canRotateLeft() {
        //Given
        Coordinates startingPosition = new Coordinates(1,2);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.turnLeft();

        //then
        Assert.assertEquals("1 2 W", marsRover.currentLocation());
    }

    @Test
    public void canRotateRight() {
        //Given
        Coordinates startingPosition = new Coordinates(1,2);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.turnRight();

        //then
        Assert.assertEquals("1 2 E", marsRover.currentLocation());
    }

    @Test
    public void canMove() {
        //Given
        Coordinates startingPosition = new Coordinates(1,2);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.move();

        //then
        Assert.assertEquals("1 3 N", marsRover.currentLocation());
    }

    @Test
    public void canRunCommandToRotateRight() {
        //Given
        Coordinates startingPosition = new Coordinates(1,2);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.run("R");

        //then
        Assert.assertEquals("1 2 E", marsRover.currentLocation());
    }

    @Test
    public void canRunCommandToRotateLeft() {
        //Given
        Coordinates startingPosition = new Coordinates(1,2);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.run("L");

        //then
        Assert.assertEquals("1 2 W", marsRover.currentLocation());
    }

    @Test
    public void canRunCommandToMove() {
        //Given
        Coordinates startingPosition = new Coordinates(1,2);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.run("M");

        //then
        Assert.assertEquals("1 3 N", marsRover.currentLocation());
    }

    @Test
    public void canRunCommandWithMultipleInstructions() {
        //Given
        Coordinates startingPosition = new Coordinates(3,3);
        MarsRover marsRover = new MarsRover(highland, Direction.E, startingPosition);

        //When
        marsRover.run("MMRMMRMRRM");

        //then
        Assert.assertEquals("5 1 E", marsRover.currentLocation());
        logger.debug("canRunCommandWithMultipleInstructions expected - 5 1 E, real {}",
            marsRover.currentLocation());

    }

    @Test
    public void wontDriveOffPlateau() {
        //Given
        Coordinates startingPosition = new Coordinates(3,3);
        MarsRover marsRover = new MarsRover(highland, Direction.N, startingPosition);

        //When
        marsRover.run("MMMMMMMMMMR");

        //then
        Assert.assertEquals("3 5 E", marsRover.currentLocation());
        logger.debug("canRunCommandWithMultipleInstructions expected - 3 5 E, real {}",
            marsRover.currentLocation());
    }
}
