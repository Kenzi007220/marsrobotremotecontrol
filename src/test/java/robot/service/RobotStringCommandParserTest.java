package robot.service;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import robot.commands.RCommand;
import robot.commands.RCommandImp.MoveCommand;
import robot.commands.RCommandImp.TurnLeftCommand;
import robot.commands.RCommandImp.TurnRightCommand;

public class RobotStringCommandParserTest {

    @Test
    public void stringLMapsToTurnLeftCommand() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser("L");

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertTrue(commands.get(0) instanceof TurnLeftCommand);
        Assert.assertEquals(1, commands.size());
    }

    @Test
    public void stringRMapsToTurnRightCommand() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser("R");

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertTrue(commands.get(0) instanceof TurnRightCommand);
    }

    @Test
    public void stringMMapsToMoveCommand() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser("M");

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertTrue(commands.get(0) instanceof MoveCommand);
    }


    @Test
    public void emptyStringResultsInEmptyCommandList() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser("");

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertEquals(0, commands.size());
    }


    @Test
    public void nullStringResultsInEmptyCommandList() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser(null);

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertEquals(0, commands.size());
    }

    @Test
    public void stringToCommandMappingIsCaseInsensitive() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser("mM");

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertTrue(commands.get(0) instanceof MoveCommand);
        Assert.assertTrue(commands.get(1) instanceof MoveCommand);
    }

    @Test
    public void multiCommandStringIsMappedToCommandsInSameOrder() {
        //Init
        RobotStringCommandParser parser = new RobotStringCommandParser("MRL");

        //If
        List<RCommand> commands = parser.toCommands();

        //Then
        Assert.assertTrue(commands.get(0) instanceof MoveCommand);
        Assert.assertTrue(commands.get(1) instanceof TurnRightCommand);
        Assert.assertTrue(commands.get(2) instanceof TurnLeftCommand);
    }

}
