package robot.commands;


import lombok.extern.log4j.Log4j;
import org.junit.Assert;
import org.junit.Test;
import robot.MarsRover;
import robot.commands.RCommandImp.MoveCommand;
import robot.commands.RCommandImp.TurnLeftCommand;
import robot.commands.RCommandImp.TurnRightCommand;
import robot.domain.Coordinates;
import robot.service.Direction;
import robot.service.Highland;

@Log4j
public class MoveAndTurnTest {

  Highland highland  = new Highland(5,5);

  @Test
  public void testThatMoveCommandMovesTheNavigableObject() {
    //init
    MoveCommand command = new MoveCommand();
    Coordinates startingPosition = new Coordinates(1,2);
    MarsRover rover = new MarsRover(highland, Direction.N, startingPosition);
    //if
    command.execute(rover);

    //Then
    Assert.assertEquals("1 3 N", rover.currentLocation());

    log.debug("testThatMoveCommandMovesTheNavigableObject expected - 1 3 N, real - "
        + rover.currentLocation());
  }

  @Test
  public void testThatRotateRightCommandRotatesTheNavigableObjectRight() {
    //init
    TurnLeftCommand leftCommand = new TurnLeftCommand();
    Coordinates startingPosition = new Coordinates(1,2);
    MarsRover rover = new MarsRover(highland, Direction.N, startingPosition);

    //if
    leftCommand.execute(rover);

    //Then
    Assert.assertEquals("1 2 W", rover.currentLocation());
    log.debug("testThatRotateRightCommandRotatesTheNavigableObjectRight expected - 1 2 W, real - "
        + rover.currentLocation());
  }


  @Test
  public void testThatRotateLeftCommandRotatesTheNavigableObjectLeft() {
    //init
    TurnRightCommand rightCommand = new TurnRightCommand();
    Coordinates startingPosition = new Coordinates(1,2);
    MarsRover rover = new MarsRover(highland, Direction.N, startingPosition);

    //if
    rightCommand.execute(rover);

    //Then
    Assert.assertEquals("1 2 E", rover.currentLocation());

    log.debug("testThatRotateRightCommandRotatesTheNavigableObjectRight expected - 1 2 E, real - "
        + rover.currentLocation());

  }



}
