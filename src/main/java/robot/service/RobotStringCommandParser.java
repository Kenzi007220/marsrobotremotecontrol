package robot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import robot.commands.RCommand;
import robot.commands.RCommandImp.MoveCommand;
import robot.commands.RCommandImp.TurnLeftCommand;
import robot.commands.RCommandImp.TurnRightCommand;

public class RobotStringCommandParser {

  public static final String BY_EACH_CHARACTER = "";
  public static final int START_INDEX = 0;

  private static Map<String, RCommand> stringToCommandMap = new HashMap<String, RCommand>() {{
    put("L", new TurnLeftCommand());
    put("R", new TurnRightCommand());
    put("M", new MoveCommand());
  }};

  private String commandString;

  public RobotStringCommandParser(final String commandString) {
    this.commandString = commandString;
  }

  public List<RCommand> toCommands() {
    if(isNullOrEmpty(commandString)) return new ArrayList<RCommand>();
    return buildCommandsList(commandString);
  }

  private List<RCommand> buildCommandsList(final String commandString) {
    List<RCommand> commands = new ArrayList<RCommand>();



    for (String commandCharacter : commandCharactersFrom(commandString)) {
      if (commandCharacter == null) {
        break;
      }
      RCommand mappedCommand = lookupEquivalentCommand(commandCharacter.toUpperCase());
      commands.add(mappedCommand);
    }

    return commands;
  }

  private boolean isNullOrEmpty(final String commandString) {
    return (null == commandString || commandString.trim().length() == 0);
  }

  private String[] commandCharactersFrom(final String commandString) {
    return Arrays
        .copyOfRange(commandString.split(BY_EACH_CHARACTER), START_INDEX, commandString.length() + 1);
  }

  private RCommand lookupEquivalentCommand(final String commandString) {
    return stringToCommandMap.get(commandString);
  }

}
