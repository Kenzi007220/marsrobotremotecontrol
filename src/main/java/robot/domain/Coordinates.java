package robot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor //Create constructors
public class Coordinates {

  private int xCoordinate;
  private int yCoordinate;


  @Override
  public String toString() {
    StringBuilder coordinateOutput = new StringBuilder();
    coordinateOutput.append(xCoordinate);
    coordinateOutput.append(" ");
    coordinateOutput.append(yCoordinate);
    return coordinateOutput.toString();
  }

  public Coordinates newCoordinatesFor(final int xCoordinateStepValue, final int yCoordinateStepValue) {
    return new Coordinates(this.xCoordinate + xCoordinateStepValue, this.yCoordinate + yCoordinateStepValue);
  }

  public boolean hasWithinBounds(final Coordinates coordinates) {
    return isXCoordinateWithinBounds(coordinates.xCoordinate) && isYCoordinateWithinBounds(coordinates.yCoordinate);
  }

  public boolean hasOutsideBounds(final Coordinates coordinates) {
    return isXCoordinateInOutsideBounds(coordinates.xCoordinate) && isYCoordinateInOutsideBounds(coordinates.yCoordinate);
  }

  private boolean isXCoordinateInOutsideBounds(final int xCoordinate) {
    return xCoordinate >= this.xCoordinate;
  }

  private boolean isYCoordinateInOutsideBounds(final int yCoordinate) {
    return yCoordinate >= this.yCoordinate;
  }

  private boolean isYCoordinateWithinBounds(final int yCoordinate) {
    return yCoordinate <= this.yCoordinate;
  }

  private boolean isXCoordinateWithinBounds(final int xCoordinate) {
    return xCoordinate <= this.xCoordinate;
  }

  public Coordinates newCoordinatesForStepSize(final int xCoordinateStepValue, final int yCoordinateStepValue) {
    return new Coordinates(xCoordinate+xCoordinateStepValue, yCoordinate+yCoordinateStepValue);
  }


}
