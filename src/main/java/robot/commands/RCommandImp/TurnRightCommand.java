package robot.commands.RCommandImp;

import robot.MarsRover;
import robot.commands.RCommand;

public class TurnRightCommand implements RCommand {

  @Override
  public void execute(final MarsRover rover) {
    rover.turnRight();
  }

}
