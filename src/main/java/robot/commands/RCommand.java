package robot.commands;

import robot.MarsRover;

public interface RCommand {

  public void execute(final MarsRover rover);

}
