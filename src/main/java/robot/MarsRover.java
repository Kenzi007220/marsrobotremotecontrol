package robot;

import java.util.List;
import lombok.Data;
import robot.commands.RCommand;
import robot.domain.Coordinates;
import robot.service.Direction;
import robot.service.Highland;
import robot.service.RobotStringCommandParser;

@Data
public class MarsRover {

  private Coordinates currentCoordinates;
  private Direction currentDirection;
  private Highland highland;


  public MarsRover(final Highland highland, final Direction direction, final Coordinates coordinates) {
    this.highland = highland;
    this.currentDirection = direction;
    this.currentCoordinates = coordinates;
  }

  public void run(final String commandString) {
    List<RCommand> roverCommands = new RobotStringCommandParser(commandString).toCommands();
    for (RCommand command : roverCommands) {
      command.execute(this);
    }
  }
  public String currentLocation() {
    return currentCoordinates.toString() + " " + currentDirection.toString();
  }

  public void turnRight() {
    this.currentDirection = this.currentDirection.right();
  }

  public void turnLeft() {
    this.currentDirection = this.currentDirection.left();
  }

  public void move() {
    Coordinates positionAfterMove = currentCoordinates.newCoordinatesForStepSize(currentDirection.stepSizeForXAxis(), currentDirection.stepSizeForYAxis());

    //ignores the command if rover off highlind
    if(highland.hasWithinBounds(positionAfterMove))
      currentCoordinates = currentCoordinates.newCoordinatesFor(currentDirection.stepSizeForXAxis(), currentDirection.stepSizeForYAxis());
  }

}
